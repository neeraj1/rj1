import React from "react";
import {connect} from "react-redux";
import {BrowserRouter as Router, Route, Link,Switch} from "react-router-dom";

const mapStateToProps = function(state){
  return { "userList": state.users };
}



const Main=(props)=>(
  <main>
  <div>

<h2>List of Users</h2>

<ul>
  {
    props.userList.map(ele => (
    <li key={ele.id}>
    <Link to=
    {`/user/${ele.id}`}>
      {ele.first_name}
    </Link>
    </li>
  ))}
</ul>
</div>
</main>
);
const Singleuser=(props)=>(
  <singleuser>{
  props.userList.map(ele => {
    if (parseInt(ele.id,10)===parseInt(props.match.params.id,10))
    return(<div key={ele.id}>
      <pre>
        <b>Id:        </b>{ele.id}<br/>
        <b>First Name:</b>{ele.first_name}<br/>
        <b>Last Name: </b>{ele.last_name}<br/>
        <b>Email:     </b>{ele.email}<br/>
        <b>Gender:    </b>{ele.gender}<br/>
      </pre>
    </div>)
    else{
      return null;
    }

  })
}
</singleuser>

)


const App=function({userList}){
  return (
    <Router>
    <Switch>
    <Route exact path="/" render={(routeProps)=><Main {...routeProps} {...{userList}}/>} />
    <Route path="/user/:id" render={(props)=><Singleuser {...props} {...{userList}}/>} />
    </Switch>
    </Router>
);
}
export default connect(mapStateToProps)(App);
