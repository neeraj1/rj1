import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import './index.css';
import store from "./store/index";
import App from "./components/App";
//import registerServiceWorker from './registerServiceWorker';
render(
  <Provider store= {store}>
  <App />
  </Provider>,
  document.getElementById('app')
);
