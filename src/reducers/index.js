const initialState={
  users:    [{
        "id": 1,
        "first_name": "Birch",
        "last_name": "Tuppeny",
        "email": "btuppeny0@umich.edu",
        "gender": "Male"
      }, {
        "id": 2,
        "first_name": "Dannie",
        "last_name": "McAlindon",
        "email": "dmcalindon1@comsenz.com",
        "gender": "Male"
      }, {
        "id": 3,
        "first_name": "Hermie",
        "last_name": "Yakovitch",
        "email": "hyakovitch2@reference.com",
        "gender": "Male"
      }, {
        "id": 4,
        "first_name": "Gwynne",
        "last_name": "Mougel",
        "email": "gmougel3@bbc.co.uk",
        "gender": "Female"
      }, {
        "id": 5,
        "first_name": "Syd",
        "last_name": "Fallis",
        "email": "sfallis4@ycombinator.com",
        "gender": "Male"
      }, {
        "id": 6,
        "first_name": "Christoffer",
        "last_name": "Tarzey",
        "email": "ctarzey5@trellian.com",
        "gender": "Male"
      }
  ]
};

function rootReducer(state=initialState,action){
  return state;
}

export default rootReducer;
